package com.dev.bootcamp;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.dev.bootcamp.Move.CHEAT;
import static com.dev.bootcamp.Move.COOPERATE;
import static org.mockito.Mockito.*;

public class GameTest {

    private Player player1 = mock(Player.class);
    private Player player2 = mock(Player.class);
    private Machine machine = mock(Machine.class);

    @Before
    public void setup() {
        when(player1.move()).thenReturn(COOPERATE);
        when(player2.move()).thenReturn(COOPERATE);
        when(machine.scoreFor(new Move[]{COOPERATE,COOPERATE})).thenReturn(new int[]{2, 2});
    }

    @Test
    public void shouldReturnInstance() {
        Game game = new Game(player1, player2, machine, 1);
        Assert.assertNotNull(game);
    }

    @Test
    public void shouldGameHavePlay() {
        Game game = new Game(player1, player2, machine, 1);
        game.play();
    }

    @Test
    public void playersShouldAbleToMove() {
        Game game = new Game(player1, player2, machine, 1);
        game.play();
        verify(player1).move();
        verify(player2).move();
    }

    @Test
    public void machineShouldBeAbleToCalculateTheScore() {
        Game game = new Game(player1, player2, machine, 1);
        game.play();
        verify(machine).scoreFor(any());
    }

    @Test
    public void playersScoreShouldBeUpdated() {
        Game game = new Game(player1, player2, machine, 1);
        when(player1.move()).thenReturn(COOPERATE);
        when(player2.move()).thenReturn(COOPERATE);
        when(machine.scoreFor(new Move[]{COOPERATE,COOPERATE})).thenReturn(new int[]{2, 2});
        game.play();
        verify(player1).addScore(2);
        verify(player2).addScore(2);
    }

    @Test
    public void playersScoreShouldBeUpdatedForCooperateCheat() {
        Game game = new Game(player1, player2, machine, 1);
        when(player1.move()).thenReturn(COOPERATE);
        when(player2.move()).thenReturn(CHEAT);
        when(machine.scoreFor(new Move[]{COOPERATE,CHEAT})).thenReturn(new int[]{-1, 3});
        game.play();
        verify(player1).addScore(-1);
        verify(player2).addScore(3);
    }

    @Test
    public void playerScoreShouldBeUpdatedForMultipleRounds() {
        Game game = new Game(player1, player2, machine, 2);
        when(player1.move()).thenReturn(COOPERATE).thenReturn(CHEAT);
        when(player2.move()).thenReturn(CHEAT).thenReturn(COOPERATE);
        when(machine.scoreFor(new Move[]{COOPERATE, CHEAT})).thenReturn(new int[]{-1, 3});
        when(machine.scoreFor(new Move[]{CHEAT, COOPERATE})).thenReturn(new int[]{3, -1});

        game.play();
        verify(player1).addScore(-1);
        verify(player2).addScore(3);

        verify(player1).addScore(3);
        verify(player2).addScore(-1);

    }

    // for 5 rounds run for cheater and coop and validate teh results
    @Test
    public void validatePlayersScoreForCheaterAndCooperatorForFiveRounds() {
        Behaviour cooperativeBehaviour = () -> {
            return Move.COOPERATE;
        };
        Player cooperative = new Player(cooperativeBehaviour);

        Behaviour cheaterBehaviour = () -> {
            return Move.CHEAT;
        };
        Player cheater = new Player(cheaterBehaviour);

        Machine machine = new Machine();

        Game game = new Game(cooperative, cheater, machine, 5);

        game.play();

        Assert.assertEquals(-5, cooperative.score());
        Assert.assertEquals(15, cheater.score());
    }

    @Test
    public void shouldValidateScoreForCopycatAndCheatPlayers() {
        CopycatBehaviour copycatBehaviour = new CopycatBehaviour();
        Player copycat = new Player(copycatBehaviour);
        Player cheater = new Player(() -> Move.CHEAT);
        Machine machine = new Machine();
        Game game = new Game(copycat, cheater, machine, 2);
        game.addObserver(copycatBehaviour);

        game.play();

        Assert.assertEquals(-1, copycat.score());
        Assert.assertEquals(3, cheater.score());

    }

    @Test
    public void shouldValidateScoreForCheatAndCopycatPlayers() {
        CopycatBehaviour copycatBehaviour = new CopycatBehaviour();
        Player copycat = new Player(copycatBehaviour);
        Player cheater = new Player(() -> Move.CHEAT);
        Machine machine = new Machine();
        Game game = new Game(cheater, copycat, machine, 2);
        game.addObserver(copycatBehaviour);

        game.play();

        Assert.assertEquals(-1, copycat.score());
        Assert.assertEquals(3, cheater.score());

    }

    @Test
    public void shouldValidateScoreForCooperateAndCopycatPlayers() {
        CopycatBehaviour copycatBehaviour = new CopycatBehaviour();
        Player copycat = new Player(copycatBehaviour);
        Player cooperate = new Player(() -> COOPERATE);
        Machine machine = new Machine();
        Game game = new Game(cooperate, copycat, machine, 2);
        game.addObserver(copycatBehaviour);

        game.play();

        Assert.assertEquals(4, copycat.score());
        Assert.assertEquals(4, cooperate.score());

    }

    @Test
    public void shouldValidateScoreForCopycatAndCooperatePlayers() {
        CopycatBehaviour copycatBehaviour = new CopycatBehaviour();
        Player copycat = new Player(copycatBehaviour);
        Player cooperate = new Player(() -> COOPERATE);
        Machine machine = new Machine();
        Game game = new Game(cooperate, copycat, machine, 2);
        game.addObserver(copycatBehaviour);

        game.play();

        Assert.assertEquals(4, copycat.score());
        Assert.assertEquals(4, cooperate.score());

    }

    @Test
    public void shouldValidateScoreForCopycatAndGrudgerPlayers() {
        CopycatBehaviour copycatBehaviour = new CopycatBehaviour();
        Player copycat = new Player(copycatBehaviour);

        GrudgeBehaviour grudgeBehaviour = new GrudgeBehaviour();
        Player grudger = new Player(grudgeBehaviour);

        Game game = new Game(copycat, grudger, new Machine(), 5);
        game.addObserver(copycatBehaviour);

        game.play();

        Assert.assertEquals(10, copycat.score());
        Assert.assertEquals(10, grudger.score());

    }

    @Test
    public void shouldValidateScoreForCheaterAndGrudgerPlayers() {
        Player cheater = new Player(() -> CHEAT);

        GrudgeBehaviour grudgeBehaviour = new GrudgeBehaviour();
        Player grudger = new Player(grudgeBehaviour);

        Game game = new Game(cheater, grudger, new Machine(), 5);
        game.addObserver(grudgeBehaviour);
        game.play();

        Assert.assertEquals(3, cheater.score());
        Assert.assertEquals(-1, grudger.score());

    }

    @Test
    public void shouldValidateScoreForCopycatPlayers() {
        CopycatBehaviour copycatBehaviour = new CopycatBehaviour();
        Player copycat1 = new Player(copycatBehaviour);

        CopycatBehaviour copycatBehaviour2 = new CopycatBehaviour();
        Player copycat2 = new Player(copycatBehaviour);

        Game game = new Game(copycat1, copycat2, new Machine(), 5);
        game.addObserver(copycatBehaviour);
        game.addObserver(copycatBehaviour2);
        game.play();

        Assert.assertEquals(10, copycat1.score());
        Assert.assertEquals(10, copycat2.score());

    }



}
