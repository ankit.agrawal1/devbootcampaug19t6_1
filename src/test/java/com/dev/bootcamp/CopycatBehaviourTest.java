package com.dev.bootcamp;

import org.junit.Assert;
import org.junit.Test;

import java.util.Observable;

public class CopycatBehaviourTest {

    @Test
    public void shouldCooperateAsFirstMove() {
        Behaviour copycatBehaviour = new CopycatBehaviour();
        Move move = copycatBehaviour.move();
        Assert.assertEquals(Move.COOPERATE, move);
    }

    @Test
    public void shouldCopyOpponentsPreviousMove() {
        CopycatBehaviour copycatBehaviour = new CopycatBehaviour();
        copycatBehaviour.move();
        copycatBehaviour.update(new Observable(), Move.CHEAT);

        Move move = copycatBehaviour.move();
        Assert.assertEquals(Move.CHEAT, move);
    }

}
