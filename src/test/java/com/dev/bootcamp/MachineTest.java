package com.dev.bootcamp;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.dev.bootcamp.Move.CHEAT;
import static com.dev.bootcamp.Move.COOPERATE;

public class MachineTest {

    private Machine machine;

    @Before
    public void setup() {
        this.machine = new Machine();
    }

    @Test
    public void shouldReturnEqualNumberOfScores() {
        int[] scores = machine.scoreFor(new Move[]{COOPERATE,COOPERATE});
        Assert.assertEquals(2,scores.length);
    }

    @Test
    public void shouldReturnScoresForMoves() {
        int[] scores = machine.scoreFor(new Move[]{COOPERATE,COOPERATE});
        Assert.assertEquals(2,scores[0]);
        Assert.assertEquals(2,scores[1]);

        int[] scores1 = machine.scoreFor(new Move[]{COOPERATE,CHEAT});
        Assert.assertEquals(-1,scores1[0]);
        Assert.assertEquals(3,scores1[1]);

        int[] scores2 = machine.scoreFor(new Move[]{CHEAT,CHEAT});
        Assert.assertEquals(0,scores2[0]);
        Assert.assertEquals(0,scores2[1]);

        int[] scores3 = machine.scoreFor(new Move[]{CHEAT,COOPERATE});
        Assert.assertEquals(3,scores3[0]);
        Assert.assertEquals(-1,scores3[1]);

    }

}
