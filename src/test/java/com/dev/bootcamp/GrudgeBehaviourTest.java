package com.dev.bootcamp;

import org.junit.Test;

import static org.junit.Assert.*;

public class GrudgeBehaviourTest {

    @Test
    public void shouldReturnCooperateAsFirstMove() {
        GrudgeBehaviour grudgeBehaviour = new GrudgeBehaviour();
        assertEquals(Move.COOPERATE, grudgeBehaviour.move());
    }

    @Test
    public void shouldAlwaysReturnCheatWhenOpponentCheated() {
        GrudgeBehaviour grudgeBehaviour = new GrudgeBehaviour();
        assertEquals(Move.COOPERATE, grudgeBehaviour.move());
        grudgeBehaviour.update(null, Move.CHEAT);
        assertEquals(Move.CHEAT, grudgeBehaviour.move());
        grudgeBehaviour.update(null, Move.COOPERATE);
        assertEquals(Move.CHEAT, grudgeBehaviour.move());
    }
}