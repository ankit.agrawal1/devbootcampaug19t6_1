package com.dev.bootcamp;

import org.junit.Assert;
import org.junit.Test;

public class DetectiveBehaviourTest {
    @Test
    public void shouldHaveFirstFourPredefinedMoves() {
        DetectiveBehaviour detectiveBehaviour = new DetectiveBehaviour();
        Assert.assertEquals(Move.COOPERATE, detectiveBehaviour.move());
        Assert.assertEquals(Move.CHEAT, detectiveBehaviour.move());
        Assert.assertEquals(Move.COOPERATE, detectiveBehaviour.move());
        Assert.assertEquals(Move.COOPERATE, detectiveBehaviour.move());
    }

    @Test
    public void shouldBeCopycatWhenOpponentCheatsInAnyFirstFourMoves() {
        DetectiveBehaviour detectiveBehaviour = new DetectiveBehaviour();
        Assert.assertEquals(Move.COOPERATE, detectiveBehaviour.move());
        detectiveBehaviour.update(null, Move.CHEAT);
        Assert.assertEquals(Move.CHEAT, detectiveBehaviour.move());
        Assert.assertEquals(Move.COOPERATE, detectiveBehaviour.move());
        Assert.assertEquals(Move.COOPERATE, detectiveBehaviour.move());

        detectiveBehaviour.update(null, Move.CHEAT);
        Assert.assertEquals(Move.COOPERATE, detectiveBehaviour.move());
    }

    @Test
    public void shouldBeCheaterWhenOpponentDoesnotCheatInAnyFirstFourMoves() {
        DetectiveBehaviour detectiveBehaviour = new DetectiveBehaviour();
        Assert.assertEquals(Move.COOPERATE, detectiveBehaviour.move());
        Assert.assertEquals(Move.CHEAT, detectiveBehaviour.move());
        Assert.assertEquals(Move.COOPERATE, detectiveBehaviour.move());
        Assert.assertEquals(Move.COOPERATE, detectiveBehaviour.move());

        detectiveBehaviour.update(null, Move.COOPERATE);
        Assert.assertEquals(Move.CHEAT, detectiveBehaviour.move());
    }
}
