package com.dev.bootcamp;

import java.util.Observable;
import java.util.Observer;

public class GrudgeBehaviour implements Behaviour, Observer {
    private boolean didOpponentCheat = false;

    @Override
    public Move move() {
        if(didOpponentCheat) {
            return Move.CHEAT;
        }
        return Move.COOPERATE;
    }

    @Override
    public void update(Observable observable, Object arg) {
        Move opponentPreviousMove = (Move) arg;
        if(opponentPreviousMove == Move.CHEAT) {
            removeObserver(observable);
            didOpponentCheat = true;
        }
    }

    private void removeObserver(Observable observable) {
        if(observable != null) {
            Game game = (Game) observable;
            game.deleteObserver(this);
        }
    }
}
