package com.dev.bootcamp;

import java.util.Observable;
import java.util.Observer;

public class DetectiveBehaviour implements Behaviour, Observer {

    private static final Move[] INITIAL_FOUR_MOVES = new Move[]{Move.COOPERATE, Move.CHEAT, Move.COOPERATE, Move.COOPERATE};
    private int moveCount = 0;
    private Behaviour behaviour;

    @Override
    public Move move() {
        if (moveCount < 4) {
            return INITIAL_FOUR_MOVES[moveCount++];
        }
        return behaviour.move();
    }

    @Override
    public void update(Observable o, Object arg) {
        Move opponentMove = (Move) arg;
        if (moveCount < 4) {
            if (opponentMove == Move.CHEAT && behaviour == null) {
                makeBehaviourAsCopycat(o, opponentMove);
                updateObservers(o);
            }
        } else {
            if (behaviour == null) {
                behaviour = () -> Move.CHEAT;
            }
        }

    }

    private void updateObservers(Observable o) {
        if (o != null) {
            o.deleteObserver(this);
            o.addObserver((CopycatBehaviour) behaviour);
        }
    }

    private void makeBehaviourAsCopycat(Observable o, Move opponentMove) {
        behaviour = new CopycatBehaviour();
    }
}
