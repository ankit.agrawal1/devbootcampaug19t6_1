package com.dev.bootcamp;

public class Application {

    public static void main(String[] args) {
        CopycatBehaviour copycatBehaviour = new CopycatBehaviour();
        Player copycat = new Player(copycatBehaviour);
        GrudgeBehaviour grudgeBehaviour = new GrudgeBehaviour();
        Player grudger = new Player(grudgeBehaviour);
        Game game = new Game(copycat, grudger, new Machine(), 5);
        game.addObserver(copycatBehaviour);
        game.addObserver(grudgeBehaviour);
        game.play();
    }

}
