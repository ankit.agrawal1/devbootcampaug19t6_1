package com.dev.bootcamp;

import static com.dev.bootcamp.Move.CHEAT;
import static com.dev.bootcamp.Move.COOPERATE;

public class Machine {

    public int[] scoreFor(Move[] moves) {
        if (moves[0] == COOPERATE && moves[1] == COOPERATE) {
            return new int[]{2, 2};
        } else if (moves[0] == COOPERATE && moves[1] == CHEAT) {
            return new int[]{-1, 3};
        } else if (moves[0] == CHEAT && moves[1] == COOPERATE) {
            return new int[]{3, -1};
        } else {
            return new int[]{0, 0};
        }
    }
}
