package com.dev.bootcamp;

import java.util.Scanner;

public interface Behaviour {
    public Move move();
}

class ConsoleBehaviour implements Behaviour {

    private Scanner scanner;

    ConsoleBehaviour(Scanner scanner) {
        this.scanner = scanner;
    }

    @Override
    public Move move() {
        return "1".equals(scanner.next()) ? Move.COOPERATE : Move.CHEAT;
    }
}