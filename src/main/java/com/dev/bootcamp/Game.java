package com.dev.bootcamp;

import java.util.Observable;
import java.util.Observer;

public class Game extends Observable {

    private final Player player1;
    private final Player player2;
    private final Machine machine;
    private final int noOfRounds;

    public Game(Player player1, Player player2, Machine machine, int noOfRounds) {
        this.player1 = player1;
        this.player2 = player2;
        this.machine = machine;
        this.noOfRounds = noOfRounds;
    }

    public void play() {
        Behaviour player1Behaviour = this.player1.getBehaviour();
        Behaviour player2Behaviour = this.player2.getBehaviour();
        for (int i = 0; i < noOfRounds; i++) {
            Move player1Move = this.player1.move();
            Move player2Move = this.player2.move();
            notifyOpponentMoveForObserver(player1Behaviour, player2Move);
            notifyOpponentMoveForObserver(player2Behaviour, player1Move);
            int[] scores = this.machine.scoreFor(new Move[]{player1Move, player2Move});
            player1.addScore(scores[0]);
            player2.addScore(scores[1]);

            System.out.println("Player 1 : " + player1.score() + " Player 2 : " + player2.score());
        }
    }

    private void notifyOpponentMoveForObserver(Behaviour playerBehaviour, Move playerMove) {
        if (playerBehaviour instanceof Observer) {
            notifyOpponentsPreviousMove(playerMove);
        }
    }

    private void notifyOpponentsPreviousMove(Move opponentsMove) {
        setChanged();
        notifyObservers(opponentsMove);
    }

}
